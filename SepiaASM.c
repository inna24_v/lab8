#include <stddef.h>
#include "lib.h"
float converter[256];

float coMatrix[36] = {0.131f, 0.168f, 0.189f, 0.131f, 0.543f, 0.686f, 0.769f, 0.543f, 0.272f, 0.349f, 0.393f, 0.272f,
                      0.168f, 0.189f, 0.131f, 0.168f, 0.686f, 0.769f, 0.543f, 0.686f, 0.349f, 0.393f, 0.272f, 0.349f,
                      0.189f, 0.131f, 0.168f, 0.189f, 0.769f, 0.543f, 0.686f, 0.769f, 0.393f, 0.272f, 0.349f, 0.393f
                     };


void sepia_sse_inplace(struct image_t* img){
    size_t frameCount = img->width*img->height/4;
    int extraPix = img->width*img->height%4;
	size_t i;
	int k;
	float con;
	for (con=0; con<256; con++) converter[(int)con] = con;
    for (i = 0; i < frameCount; i++){

        size_t j = 4*i;

		float data[36] = {converter[img->data[j].b], converter[img->data[j].b], converter[img->data[j].b], converter[img->data[j+1].b],
                          converter[img->data[j].g], converter[img->data[j].g], converter[img->data[j].g], converter[img->data[j+1].g],
                          converter[img->data[j].r], converter[img->data[j].r], converter[img->data[j].r], converter[img->data[j+1].r],

                          converter[img->data[j+1].b], converter[img->data[j+1].b], converter[img->data[j+2].b], converter[img->data[j+2].b],
                          converter[img->data[j+1].g], converter[img->data[j+1].g], converter[img->data[j+2].g], converter[img->data[j+2].g],
                          converter[img->data[j+1].r], converter[img->data[j+1].r], converter[img->data[j+2].r], converter[img->data[j+2].r],

                          converter[img->data[j+2].b], converter[img->data[j+3].b], converter[img->data[j+3].b], converter[img->data[j+3].b],
                          converter[img->data[j+2].g], converter[img->data[j+3].g], converter[img->data[j+3].g], converter[img->data[j+3].g],
                          converter[img->data[j+2].r], converter[img->data[j+3].r], converter[img->data[j+3].r], converter[img->data[j+3].r],
                          };

        sse(data, coMatrix, &(img->data[j]));

    }

    for (k = 0; k < extraPix; ++k) {
        sepia_one(&(img->data[frameCount+k]));
    }

    return;

}
