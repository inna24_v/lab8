C_FLAGS = -lm -g -c
ASM_FLAGS = -felf64 -o
main: main.c bmpio.c SepiaC.c SepiaASM.c sse.asm lib.h
	nasm $(ASM_FLAGS) sse.o sse.asm
	gcc $(C_FLAGS) bmpio.c -o bmpio.o
	gcc $(C_FLAGS) SepiaC.c -o SepiaC.o
	gcc $(C_FLAGS) SepiaASM.c -o SepiaASM.o
	gcc $(C_FLAGS) main.c -o main.o
	gcc -g -lm *.o -o main
	rm -rf *.o


delete:
	rm test_sepia_*
