#include <stdio.h>
#include <stdlib.h>
#include "lib.h"
enum read_status_t from_bmp(FILE* in, struct image_t* const read){
    struct bmp_header* header = malloc(sizeof(struct bmp_header));
	char devnull[3];
	int i;
	size_t dummyZeroes = 0;
    if (fread(header, sizeof(struct bmp_header), 1, in) != 1) return READ_INVALID_HEADER;
    read->height = header->biHeight;
    read->width = header->biWidth;
    read->data = (struct pixel_t*) malloc(sizeof(struct pixel_t)*read->height*read->width);

    while ((read->width*3 + dummyZeroes)%4){
        dummyZeroes++;
    }

    for (i = 0; i < read->height; i++){
            if(fread(read->data + i*read->width, sizeof(struct pixel_t),read->width,in) != read->width) return READ_INVALID_BITS;
            fread(devnull, sizeof(int8_t), dummyZeroes, in);
    }
    return READ_OK;
}

enum write_status_t to_bmp(FILE* out, struct image_t* const img){
    struct bmp_header header;
	int dummyZeroes = 0;
    int8_t zeroes[3] = {0,0,0};
	int i;
    header.bfType = 19778;
    header.bFileSize = img->width*img->height*3 + 54;
    header.bfReserved = 0;
    header.b0ffBits = 54;

    header.biSize = 40;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = img->width*img->height*3;
    header.biXPelsPerMeter = 3780;
    header.biYPelsPerMeter = 3780;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    fwrite(&header, sizeof(struct bmp_header), 1, out);
    
    while ((img->width*3 + dummyZeroes)%4){
        dummyZeroes++;
    }
    for (i = 0; i < img->height; ++i) {
        fwrite(img->data + i*img->width, sizeof(struct pixel_t), img->width, out);
        fwrite(zeroes, sizeof(int8_t), dummyZeroes, out);
    }
    return WRITE_OK;
}
