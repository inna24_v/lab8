#ifndef PICTURNER_IMAGE_ROT_STUB_H
#define PICTURNER_IMAGE_ROT_STUB_H

#include <stdio.h>
#include <stdint.h>

#pragma pack(push, 1)
struct bmp_header {
    
    uint16_t bfType; 
    uint32_t bFileSize;
    uint32_t bfReserved;
    uint32_t b0ffBits;

    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

struct pixel_t {
	uint8_t b,g,r;
} pixel_t;

struct image_t {
    int32_t width, height;
    struct pixel_t* data;
};

enum read_status_t {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum read_status_t from_bmp(FILE* in, struct image_t* const read);

struct image_t rotate(struct image_t const source, int angle);

/* serializer */
enum write_status_t{
    WRITE_OK = 0,
    WRITE_ERROR = 1
};

enum write_status_t to_bmp(FILE* out, struct image_t* const img);

void sepia_sse_inplace(struct image_t* img);
void sepia_c_inplace( struct image_t* img );
void sepia_one( struct pixel_t* const pixel );

#endif
