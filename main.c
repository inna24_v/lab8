#include <stdlib.h>
#include <string.h>
#include "stdio.h"
#include <sys/time.h>
#include <sys/resource.h>
#include "lib.h"
int main() {
    FILE* file;
    struct image_t* img = (struct image_t*)malloc(sizeof(struct image_t));
    char* filepath = "1_1.bmp";
    char* IOmode = "r+b";

    struct rusage r;
    struct timeval start;
    struct timeval end;

    getrusage(RUSAGE_SELF, &r);
    start = r.ru_utime;

    file = fopen(filepath, IOmode);
	printf("1\n");
    if(from_bmp(file, img) == 0)
		printf("READ_OK\n");
	printf("2\n");
    fclose(file);
	printf("3\n");
    sepia_c_inplace(img);

    filepath = "1_1.bmp";
    file = fopen(filepath,IOmode);
    to_bmp(file, img);
    fclose(file);
    
    getrusage(RUSAGE_SELF, &r);
    end = r.ru_utime;

    long res = ((end.tv_sec - start.tv_sec)*1000000L) + end.tv_usec - start.tv_usec;
    printf("Time elapsed in microseconds: %ld\n", res);

    return 0;
}
