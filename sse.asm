section .txt
global sse

;rdi - pointer to data
;rsi - pointer to coefs values
;rdx - output buffer

sse:
    xor eax, eax;
    xor rcx, rcx;
    .loop:
    ;moving data to registers
    movdqa xmm0, [rdi+rcx]
    movdqa xmm1, [rdi+rcx+16]
    movdqa xmm2, [rdi+rcx+32]

    ;calculate new values
    mulps xmm0, [rsi+rcx]
    mulps xmm1, [rsi+rcx+16]
    mulps xmm2, [rsi+rcx+32]
    addps xmm0, xmm1
    addps xmm0, xmm2

    ;save data
    cvtps2dq xmm0, xmm0
    packssdw xmm0, xmm1
    packuswb xmm0, xmm1
    movd [rdx+rax], xmm0

    add rax,4
    add rcx,48
    cmp rax, 12
    jl .loop

    ret
